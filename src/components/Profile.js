import React, { Component } from 'react'
import { getCookie, setCookie } from '../helpers/cookie'
import userLogo from '../assets/images/user.png'
import AddAPhotoIcon from '@material-ui/icons/AddAPhoto';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import API from '../constants/API'
import {
    NeuDiv,
    NeuButton,
    NeuTextInput,
    NeuSlider
} from "neumorphism-react";
import {
    TextNeu,
    SkeletonCustom,
    UploadCustom
} from '../components/General.Style'
import {
    mainColor,
    secondColor,
} from '../constants/Color'
import { Input, Modal, Upload, Spin } from 'antd';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import PhotoCamera from '@material-ui/icons/PhotoCamera';
import Slider from '@material-ui/core/Slider'
import Cropper from 'react-easy-crop'
import getCroppedImg from './cropImage'
import { LoadingOutlined } from '@ant-design/icons';

const { TextArea } = Input;
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

class Profile extends Component {

    state = {
        userLogin: getCookie('user_profile') != undefined || null ? JSON.parse(getCookie('user_profile')) : null,
        visibleBio: false,
        galleryTotal: this.props.galleryTotal,
        profileInfo: this.props.profileInfo,
        bioInput: '',
        editProfileVisible: false,
        editName: '',
        first_name_edit: '',
        last_name_edit: '',
        onCursorImageProfile: false,
        visibleUpdateProfilePicture: false,
        update_img: {
            file: null,
            imageSrc:
                'https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000',
            crop: { x: 0, y: 0 },
            zoom: 1,
            aspect: 1,
            croppedAreaPixels: null,
            croppedImage: null
        },
        uploadProfilePictureSpining: false,
    }

    componentWillMount() {
        if (getCookie('user_profile') === undefined || getCookie('user_profile') === null) {
            this.props.history.push("/")
        }
    }

    onCancel = () => {
        this.setState({
            editProfileVisible: false
        })
    }

    // onPreview = async file => {
    //     let src = file.url;
    //     if (!src) {
    //         src = await new Promise(resolve => {
    //             const reader = new FileReader();
    //             reader.readAsDataURL(file.originFileObj);
    //             reader.onload = () => resolve(reader.result);
    //         });
    //     }
    //     const image = new Image();
    //     image.src = src;
    //     const imgWindow = window.open(src);
    //     imgWindow.document.write(image.outerHTML);
    // };

    saveBio = async () => {
        let formData = new FormData();
        formData.append("bio", this.state.bioInput);
        try {
            const response = await API.UpdateUser(formData)
            setCookie('user_profile', {
                ...this.state.userLogin,
                bio: response.data.data.bio,
            })
            this.setState({
                visibleBio: false,
                userLogin: {
                    ...this.state.userLogin,
                    bio: response.data.data.bio,
                }
            })
        } catch (err) {
            console.log("err: ", err);
        }
    }

    saveProfile = async () => {
        let formData = new FormData(), first_name_edit = this.state.first_name_edit, last_name_edit = this.state.last_name_edit
        if (first_name_edit === '') {
            first_name_edit = this.state.userLogin.first_name
        }
        if (last_name_edit === '') {
            last_name_edit = this.state.userLogin.last_name
        }
        console.log("this.state.first_name_edit: ", this.state.first_name_edit);
        formData.append("first_name", first_name_edit);
        formData.append("last_name", last_name_edit);
        try {
            const response = await API.UpdateUser(formData)

            setCookie('user_profile', {
                ...this.state.userLogin,
                first_name: response.data.data.first_name,
                last_name: response.data.data.last_name,
            })
            window.location.reload()
        } catch (err) {
            console.log("err: ", err);
        }
        this.setState({
            editProfileVisible: false
        })
    }

    onChangeProfilePicture = ({ fileList: newFileList }) => {
        console.log("new file: ", newFileList[0]);
        const formData = new FormData();
        formData.append("photo", newFileList[0]);
    }

    handleFileChange = (e) => {
        e.preventDefault();
        let reader = new FileReader();
        let file = e.target.files[0];
        console.log("file00: ", file);

        reader.onloadend = () => {
            this.setState({
                visibleUpdateProfilePicture: true,
                update_img: {
                    ...this.state.update_img,
                    crop: { x: 0, y: 0 },
                    file: file,
                    imageSrc: reader.result
                }
            });
        }

        reader.readAsDataURL(file)
        // if (e.target.files.length >= 0) {
        //     this.setState({
        //         visibleUpdateProfilePicture: true,
        //         update_img: {
        //             ...this.state.update_img,
        //             imageSrc: 
        //         }
        //     })
        // }
    }

    onCropChange = crop => {
        this.setState({
            update_img: {
                ...this.state.update_img,
                crop: crop
            }
        })
    }

    // onCropComplete = (croppedArea, croppedAreaPixels) => {
    //     console.log("onCropComplete: ", croppedAreaPixels.width / croppedAreaPixels.height)
    // }

    onCropComplete = (croppedArea, croppedAreaPixels) => {
        console.log(croppedArea)
        console.log("croppedAreaPixels: ", croppedAreaPixels);

        this.setState({
            update_img: {
                ...this.state.update_img,
                croppedAreaPixels: croppedAreaPixels
            }
        })
    }

    onZoomChange = zoom => {
        this.setState({
            update_img: {
                ...this.state.update_img,
                zoom: zoom
            }
        })
    }

    saveProfilePicture = async () => {
        this.setState({
            uploadProfilePictureSpining: true
        })
        const croppedImage = await getCroppedImg(
            this.state.update_img.imageSrc,
            this.state.update_img.croppedAreaPixels
        )
        console.log('done', { croppedImage })
        const form = new FormData();
        // var test = new Blob
        var test = new File([croppedImage], this.state.update_img.file.name, { type: this.state.update_img.file.type, lastModified: Date.now() });
        form.append("photo", test);
        try {
            const response = await API.UpdateUser(form)
            console.log("resssss: ", response.data.data);
            await setCookie('user_profile', {
                ...this.state.userLogin,
                url: response.data.data.url,
            })
            this.setState({
                visibleUpdateProfilePicture: false,
                uploadProfilePictureSpining: false,
                userLogin: {
                    ...this.state.userLogin,
                    url: response.data.data.url
                }
            })
            // window.location.reload()
        } catch (err) {
            console.log(err);
        }
    }

    render() {
        return (
            <div
                style={{
                    height: '100%', width: '50%', minHeight: 280,
                    backgroundColor: '', position: 'relative', margin: 30, borderRadius: 30,
                    justifyContent: 'center', alignItems: 'flex-start', display: 'flex',
                }}
            >
                {/* Photo profile */}
                <div
                    style={{
                        width: '30%', height: '100%', minHeight: 280, marginTop: 14,
                        backgroundColor: '',
                        justifyContent: 'center', alignItems: 'flex-start', display: 'flex',
                    }}
                >
                    <NeuDiv
                        style={{
                            margin: 0,
                            justifyContent: 'center', alignItems: 'center', display: 'flex', backgroundColor: ''
                        }}
                        radius={90}
                        distance={4}
                        width="180px" height="180px" color="#F1F1F1"
                        onMouseOver={() => this.setState({ onCursorImageProfile: true })}
                        onMouseLeave={() => this.setState({ onCursorImageProfile: false })}
                    >
                        <img
                            style={{ borderRadius: 100 }}
                            width="160px" height="160px"
                            src={this.state.userLogin.url === '' ? userLogo : `http://localhost:3350/${this.state.userLogin.url}`} alt="Logo"
                        />

                        <div style={{ position: 'absolute', backgroundColor: '' }}>
                            <input onChange={this.handleFileChange} accept="image/*" style={{ display: 'none' }} id="icon-button-file" type="file" />
                            <label htmlFor="icon-button-file" >
                                {this.state.onCursorImageProfile === false ? null :
                                    <IconButton style={{ width: '160px', height: '160px', backgroundColor: 'rgba(0,0,0,0.3)' }} color="#ffffff" aria-label="upload picture" component="span">
                                        <PhotoCamera style={{ color: '#ffffff' }} />
                                    </IconButton>
                                }
                            </label>
                        </div>
                    </NeuDiv>
                </div>
                {/* Profile info */}
                <div
                    style={{
                        width: '90%', height: '100%', minHeight: 280,
                        backgroundColor: '',
                        justifyContent: 'flex-end', alignItems: 'flex-start', display: 'flex',
                    }}
                >
                    {/* Profile Detail */}
                    <div style={{ width: '92%', height: '100%', minHeight: 280, backgroundColor: '' }}>
                        {/* Full-Name */}
                        <div
                            style={{
                                backgroundColor: '', minHeight: 56, height: '20%', width: '100%',
                                justifyContent: 'flex-start', alignItems: 'center', display: 'flex',
                            }}
                        >
                            <TextNeu fontSize="28px" color="#262626" >
                                {this.state.userLogin.first_name}{`\xa0\xa0`}{this.state.userLogin.last_name}
                            </TextNeu>
                            <NeuButton
                                style={{
                                    margin: 0, marginLeft: 50,
                                    justifyContent: 'center', alignItems: 'center', display: 'flex',
                                }}
                                width="124px" distance={3}
                                height="40px"
                                onClick={() => this.setState({ editProfileVisible: true })}
                                color="#F1F1F1"
                            >
                                <EditOutlinedIcon style={{ color: '#262626', marginLeft: 4, marginRight: 4 }} />
                                <TextNeu fontSize="14px" color="#262626" >Edit profile</TextNeu>
                            </NeuButton>
                        </div>
                        <div
                            style={{
                                backgroundColor: '', minHeight: 223.99, height: '80%', width: '100%',
                            }}
                        >
                            <div
                                style={{
                                    width: '100%', minHeight: 67.19, height: '30%', backgroundColor: '',
                                    justifyContent: 'flex-start', alignItems: 'flex-end', display: 'flex',
                                }}
                            >
                                <NeuButton
                                    width="100px" height="80%" color="#F1F1F1" radius={10}
                                    style={{
                                        margin: 0, justifyContent: 'flex-start', alignItems: 'center',
                                        flexDirection: 'column', display: 'flex', minHeight: 53.75,
                                    }}
                                >
                                    <text style={{ fontSize: "15px", color: `${secondColor}`, fontWeight: "bold" }}>{this.state.galleryTotal}</text>
                                    <text style={{ fontSize: "15px", color: `${secondColor}`, fontWeight: "bold" }}>Gallery</text>
                                </NeuButton>
                                <NeuButton
                                    width="100px" height="80%" color="#F1F1F1" radius={10}
                                    style={{
                                        margin: 0, marginLeft: 24, justifyContent: 'flex-start', alignItems: 'center',
                                        flexDirection: 'column', display: 'flex', minHeight: 53.75,
                                    }}
                                >
                                    <text style={{ fontSize: "15px", color: `${secondColor}`, fontWeight: "bold" }}>{this.state.profileInfo.image_total}</text>
                                    <text style={{ fontSize: "15px", color: `${secondColor}`, fontWeight: "bold" }}>Images</text>
                                </NeuButton>
                                <NeuButton
                                    width="100px" height="80%" color="#F1F1F1" radius={10}
                                    style={{
                                        margin: 0, marginLeft: 24, justifyContent: 'flex-start', alignItems: 'center',
                                        flexDirection: 'column', display: 'flex', minHeight: 53.75,
                                    }}
                                >
                                    <text style={{ fontSize: "15px", color: `${secondColor}`, fontWeight: "bold" }}>129</text>
                                    <text style={{ fontSize: "15px", color: `${secondColor}`, fontWeight: "bold" }}>Likes</text>
                                </NeuButton>
                            </div>
                            <div
                                style={{
                                    backgroundColor: '',
                                    width: '100%', minHeight: 156.8, height: '70%',
                                    justifyContent: 'flex-end', alignItems: 'flex-end', display: 'flex',
                                }}>
                                <div
                                    style={{
                                        backgroundColor: '', marginTop: 41.36, marginBottom: 31.36,
                                        width: '100%', minHeight: 146.8, height: '80%',
                                        justifyContent: 'flex-start', alignItems: 'flex-start', display: 'flex',
                                    }}
                                >
                                    {
                                        this.state.visibleBio == false ?

                                            this.state.userLogin.bio != '' ?
                                                <text
                                                    style={{ fontSize: "15px", color: `${secondColor}`, fontWeight: "500", cursor: "pointer" }}
                                                    onClick={() => {
                                                        this.setState({
                                                            visibleBio: true
                                                        })
                                                    }}
                                                >
                                                    {this.state.userLogin.bio}
                                                </text>
                                                :
                                                <text
                                                    style={{ fontSize: "15px", color: '#BABABA', fontWeight: "500", cursor: "pointer" }}
                                                    onClick={() => {
                                                        this.setState({
                                                            visibleBio: true
                                                        })
                                                    }}
                                                >
                                                    Enter Your Bio...
                                                 </text>
                                            :
                                            <div
                                                style={{
                                                    height: '100%', width: '100%',
                                                    justifyContent: 'flex-start', alignItems: 'flex-start', display: 'flex',
                                                    flexDirection: 'column'
                                                }}
                                            >
                                                <TextArea
                                                    autoFocus
                                                    rows={2}
                                                    placeholder="Enter Your Bio..."
                                                    defaultValue={this.state.userLogin.bio}
                                                    onChange={(value) => this.setState({ bioInput: value.target.value })}
                                                    style={{
                                                        borderRadius: 8,
                                                        width: '80%',
                                                        backgroundColor: '#F1F1F1',
                                                        borderColor: '#F1F1F1',
                                                        boxShadow: 'inset 3px 3px 6px #cdcdcd, inset -5px -5px 10px #ffffff'
                                                    }}
                                                />
                                                <div
                                                    style={{
                                                        justifyContent: 'flex-end', alignItems: 'flex-end', display: 'flex',
                                                        flexDirection: 'row', marginTop: 26,
                                                    }}
                                                >
                                                    <NeuButton
                                                        style={{
                                                            margin: 0, marginRight: 10,
                                                            justifyContent: 'center', alignItems: 'center', display: 'flex',
                                                        }}
                                                        distance={2}
                                                        width="100px"
                                                        height="36px"
                                                        onClick={() => this.setState({ visibleBio: false, bioInput: '' })}
                                                        color="#F1F1F1"
                                                    >
                                                        <text
                                                            style={{ fontSize: "15px", color: `${mainColor}`, fontWeight: "500" }}
                                                        >
                                                            Cancel
                                                        </text>
                                                    </NeuButton>
                                                    <div
                                                        style={{
                                                            width: "100px",
                                                            height: "36px",
                                                            backgroundColor: mainColor,
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                            display: 'flex',
                                                            borderRadius: 28
                                                        }}
                                                    >
                                                        <NeuButton
                                                            style={{
                                                                margin: 0,
                                                                justifyContent: 'center', alignItems: 'center', display: 'flex',
                                                            }}
                                                            distance={2}
                                                            width="92px"
                                                            height="28px"
                                                            onClick={() => this.saveBio()}
                                                            color={mainColor}
                                                        >
                                                            <text
                                                                style={{ fontSize: "15px", color: '#ffffff', fontWeight: "500" }}
                                                            >
                                                                Save
                                                            </text>
                                                        </NeuButton>
                                                    </div>
                                                </div>
                                            </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Modal
                    title={
                        <TextNeu fontSize="20px" color={mainColor} fontWeight="bold">
                            Edit Profile
                        </TextNeu>
                    }
                    onCancel={() => this.onCancel()}
                    footer={false}
                    visible={this.state.editProfileVisible}
                >
                    <div style={{
                        backgroundColor: '', width: '100%', height: '100%',
                        justifyContent: '', alignItems: '', display: 'flex',
                        flexDirection: 'column'
                    }}>
                        <div
                            style={{
                                justifyContent: 'center', alignItems: 'center', display: 'flex',
                                flexDirection: 'row', width: '100%'
                            }}
                        >
                            <label style={{ marginRight: 10 }}>First Name: </label>
                            <Input
                                defaultValue={this.state.userLogin.first_name}
                                placeholder="Enter your first name"
                                autoFocus
                                style={{
                                    borderRadius: 30,
                                    borderColor: '#ffffff',
                                    backgroundColor: '#ffffff',
                                    boxSizing: 'border-box',
                                    border: '4px solid #ffffff',
                                    boxShadow: '2px 2px 4px #d1d1d1, -2px -2px 4px #ffffff, inset 2px 2px 4px #d1d1d1, inset -2px -2px 4px #ffffff',
                                    width: '80%', height: '50px',
                                    marginBottom: 10,
                                }}
                                onChange={(value) => this.setState({ first_name_edit: value.target.value })}
                            />
                        </div>
                        <div
                            style={{
                                justifyContent: 'center', alignItems: 'center', display: 'flex',
                                flexDirection: 'row', width: '100%'
                            }}
                        >
                            <label style={{ marginRight: 10 }}>Last Name: </label>
                            <Input
                                defaultValue={this.state.userLogin.last_name}
                                placeholder="Enter your first name"
                                style={{
                                    borderRadius: 30,
                                    borderColor: '#ffffff',
                                    backgroundColor: '#ffffff',
                                    boxSizing: 'border-box',
                                    border: '4px solid #ffffff',
                                    boxShadow: '2px 2px 4px #d1d1d1, -2px -2px 4px #ffffff, inset 2px 2px 4px #d1d1d1, inset -2px -2px 4px #ffffff',
                                    width: '80%', height: '50px',
                                }}
                                onChange={(value) => this.setState({ last_name_edit: value.target.value })}
                            />
                        </div>
                    </div>
                    <div style={{ marginTop: 24, flexDirection: 'row', display: 'flex', backgroundColor: '', justifyContent: 'flex-end' }}>
                        <NeuButton
                            style={{
                                margin: 0, marginRight: 10,
                                justifyContent: 'center', alignItems: 'center', display: 'flex',
                            }}
                            distance={2}
                            width="100px"
                            height="36px"
                            onClick={() => this.setState({ editProfileVisible: false })}
                            color="#F1F1F1"
                        >
                            <text
                                style={{ fontSize: "15px", color: `${mainColor}`, fontWeight: "500" }}
                            >
                                Cancel
                            </text>
                        </NeuButton>
                        <div
                            style={{
                                width: "100px",
                                height: "36px",
                                backgroundColor: mainColor,
                                alignItems: 'center',
                                justifyContent: 'center',
                                display: 'flex',
                                borderRadius: 28
                            }}
                        >
                            <NeuButton
                                style={{
                                    margin: 0,
                                    justifyContent: 'center', alignItems: 'center', display: 'flex',
                                }}
                                distance={2}
                                width="92px"
                                height="28px"
                                onClick={() => this.saveProfile()}
                                color={mainColor}
                            >
                                <text
                                    style={{ fontSize: "15px", color: '#ffffff', fontWeight: "500" }}
                                >
                                    Save
                                </text>
                            </NeuButton>

                        </div>
                    </div>
                </Modal>
                <Modal
                    width="35%"
                    title={
                        <TextNeu fontSize="20px" color={mainColor} fontWeight="bold">
                            Update Profile Picture
                        </TextNeu>
                    }
                    onCancel={() => this.setState({ visibleUpdateProfilePicture: false })}
                    footer={false}
                    visible={this.state.visibleUpdateProfilePicture}
                >
                    <div style={{ height: 580, justifyContent: 'center', alignItems: 'flex-start', display: 'flex', backgroundColor: "" }}>
                        <div style={{ width: '500px', height: '500px', marginTop: 20 }}>
                            <div style={{ position: 'absolute', width: '500px', height: '500px' }}>
                                <Cropper
                                    image={this.state.update_img.imageSrc}
                                    crop={this.state.update_img.crop}
                                    zoom={this.state.update_img.zoom}
                                    aspect={this.state.update_img.aspect}
                                    cropShape="round"
                                    showGrid={false}
                                    onCropChange={this.onCropChange}
                                    onCropComplete={this.onCropComplete}
                                    onZoomChange={this.onZoomChange}
                                />
                            </div>
                            <div style={{ position: 'absolute', top: '82%', width: '500px' }} >
                                <Slider
                                    value={this.state.update_img.zoom}
                                    min={1}
                                    max={3}
                                    step={0.1}
                                    // aria-labelledby="Zoom"
                                    onChange={(e, zoom) => this.onZoomChange(zoom)}
                                // classes={{ container: 'slider' }}
                                />
                            </div>
                        </div>
                    </div>
                    <div
                        style={{
                            width: '100%', height: 70, backgroundColor: '', flexDirection: 'row', display: 'flex',
                            justifyContent: 'flex-end', alignItems: 'center'
                        }}
                    >
                        <NeuButton
                            style={{
                                margin: 0, marginRight: 10,
                                justifyContent: 'center', alignItems: 'center', display: 'flex',
                            }}
                            distance={2}
                            width="100px"
                            height="36px"
                            onClick={() => this.setState({ visibleUpdateProfilePicture: false })}
                            color="#F1F1F1"
                        >
                            <text
                                style={{ fontSize: "15px", color: `${mainColor}`, fontWeight: "500" }}
                            >
                                Cancel
                            </text>
                        </NeuButton>
                        <div
                            style={{
                                width: "100px",
                                height: "36px",
                                backgroundColor: mainColor,
                                alignItems: 'center',
                                justifyContent: 'center',
                                display: 'flex',
                                borderRadius: 28
                            }}
                        >
                            <NeuButton
                                style={{
                                    margin: 0,
                                    justifyContent: 'center', alignItems: 'center', display: 'flex',
                                }}
                                distance={2}
                                width="92px"
                                height="28px"
                                onClick={() => this.saveProfilePicture()}
                                color={mainColor}
                            >
                                <text
                                    style={{ fontSize: "15px", color: '#ffffff', fontWeight: "500" }}
                                >
                                    <Spin indicator={antIcon} spinning={this.state.uploadProfilePictureSpining} />{`\xa0`}{`\xa0`}Save
                                </text>
                            </NeuButton>

                        </div>
                    </div>
                </Modal>
            </div >
        )
    }
}

export default Profile