import React, { Component } from 'react';
import logo from '../assets/images/logo.png'
import userLogo from '../assets/images/user.png'
import {
    NeuDiv,
    NeuTextInput,
    NeuButton,
    NeuToggle,
} from "neumorphism-react";
import {
    TextNeu,
    LogoText,
} from '../components/General.Style'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { Switch } from 'neumorphic-react'
import {
    mainColor,
    secondColor,
    failColor
} from '../constants/Color'
import {
    REGISTER_PAGE_PATH,
    PROFILE_PAGE_PATH
} from '../constants/RouteName'
import Collapse from '@material-ui/core/Collapse';
import { Form } from 'antd';
import axios from 'axios'
import { setCookie, getCookie } from '../helpers/cookie'

class Profile extends Component {

    state = {
        showMenu: false,
        userLogin: getCookie('user_profile') != undefined || null ? JSON.parse(getCookie('user_profile')) : null,
    }

    componentWillMount() {
        if (getCookie('user_profile') === undefined || getCookie('user_profile') === null) {
            this.props.history.push("/")
        }
    }

    componentDidMount() {
        console.log("Testttt999");

    }

    render() {
        return (
            // <div
            //     style={{
            //         width: '100%', height: '5%',
            //         flexDirection: 'row', display: 'flex', flex: 1,
            //         backgroundColor: '#F1F1F1'
            //     }}
            // >
            <NeuDiv
                radius={0}
                distance={3}
                width="100%" height="6%" color="#F1F1F1"
                style={{
                    flexDirection: 'row', display: 'flex', flex: 1,
                }}
            >
                <div
                    style={{
                        width: '10%',
                        height: '100%',
                        // backgroundColor: 'red',
                        alignItems: 'center', justifyContent: 'flex-end',
                        flex: 1, display: 'flex',
                        cursor: 'pointer',
                    }}
                    onClick={() => this.props.history.push('/')}
                >
                    <LogoText fontSize="30px" fontWeight="bold">
                        Poomstagram
                    </LogoText>
                </div>
                <div
                    style={{
                        width: '73%', height: '100%', minHeight: '50px',
                        // backgroundColor: 'blue',
                        justifyContent: 'center', alignItems: 'center', display: 'flex'
                    }}
                >
                    <NeuTextInput
                        style={{ border: 0 }}
                        placeholder="Search"
                        color="#F1F1F1"
                        onChange={(newValue) => console.log("newValue : ", newValue)}
                        width="480px"
                        height="30px"
                        distance={2}
                        fontSize={15}
                        fontColor="#151515"
                    />
                </div>
                <div
                    style={{
                        width: '17%', height: '100%',
                        backgroundColor: '',
                        justifyContent: 'flex-end', alignItems: 'center', display: 'flex'
                    }}
                >
                    <div
                        style={{
                            width: '90%',
                            justifyContent: 'flex-end', alignItems: 'center', display: 'flex', backgroundColor: ''
                        }}
                    >
                        <img
                            style={{ borderRadius: 100, marginRight: 8 }}
                            width="32px" height="32px"
                            src={userLogo} alt="Logo"
                        />
                        <TextNeu fontSize="16px" fontWeight="bold" color={secondColor}>
                            {this.state.userLogin.first_name}{`\xa0\xa0`}{this.state.userLogin.last_name}
                        </TextNeu>
                        <NeuButton
                            style={{
                                margin: 0, marginLeft: 10,
                                justifyContent: 'center', alignItems: 'center', display: 'flex',
                                background:
                                    this.state.showMenu == false ?
                                        "" : 'linear-gradient(145deg, #d9d9d9, #ffffff)',
                            }}
                            distance={3}
                            width="40px"
                            height="40px"
                            onClick={() => this.setState({ showMenu: !this.state.showMenu })}
                            color="#F1F1F1"
                        >
                            <ArrowDropDownIcon style={{ fontSize: 32 }} />
                        </NeuButton>
                    </div>
                </div>
            </NeuDiv>
            // </div>
        )
    }
}

export default Profile