import styled from 'styled-components'
import { Col, Alert, message, Card, Upload } from 'antd';
import {
  failColor,
  secondColor,
  mainColor
} from '../constants/Color'
import Image from 'react-image-resizer';

// LG = Login

// 5f4a8b

export const Container = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;
  align-items: center;
  justify-content: center;
  display: flex;
  background-color: #F1F1F1;
`

export const LGContentHeader = styled.div`
  height: ${(props) => props.height ? props.height : '20%'};
  width: 100%;
  align-items: center;
  justify-content: center;
  display: flex;
  flex-direction: column;
`

export const LGContentMiddle = styled.div`
  height: ${(props) => props.height ? props.height : '45%'};
  width: 100%;
  align-items: center;
  justify-content: ${(props) => props.justifyContent ? props.justifyContent : 'center'};
  display: flex;
  flex-direction: column;
  // background-color: red;
`

export const LGContentFooter = styled.div`
  height: ${(props) => props.height ? props.height : '35%'};
  width: 100%;
  align-items: center;
  justify-content: ${(props) => props.justifyContent ? props.justifyContent : ''};
  display: flex;
  flex-direction: column;
`


export const CustomCol = styled(Col)`
  height: ${(props) => props.height ? props.height : '110px'};
`

export const AlertNeu = styled(Alert)`
  border-radius: 8px;
  border-color: #F1F1F1;
  background: #F1F1F1;
  box-shadow: 7px 7px 14px #d6d6d6, -7px -7px 14px #F1F1F1;
  height: 100%;
  width: 100%;
  .ant-alert-message {
    color: ${failColor};
  }
`

export const TextNeu = styled.text`
  font-size: ${(props) => props.fontSize ? props.fontSize : '16px'};
  font-weight: ${(props) => props.fontWeight ? props.fontWeight : ''};
  color: ${(props) => props.color ? props.color : '#A9A9A9'};
  text-shadow: 3px 3px 3px #cdcdcd,-3px -3px 3px #F1F1F1;
`

export const LogoText = styled.text`
  font-size: ${(props) => props.fontSize ? props.fontSize : '16px'};
  font-weight: ${(props) => props.fontWeight ? props.fontWeight : ''};
  color: ${(props) => props.color ? props.color : mainColor};
  text-shadow: 3px 4px 3px #cdcdcd,-3px 4px 3px #F1F1F1;
  font-family: DancingScript;
`

export const CardCustom = styled(Card)`
  .ant-card-body {
    padding: 0px;
  }
`

export const UploadCustom = styled(Upload)`
  margin-left: 10px;
  margin-top: 3px;
  position: absolute;
  .ant-upload.ant-upload-select-picture-card {
    background-color: rgba(0,0,0,0.4);
    border-radius: 90px;
    width: 160px;
    height: 160px;
    border: 0px;
    
  }
`

export const ImageCustom = styled(Image)`
  element.style {
    position: initial;
  }
`