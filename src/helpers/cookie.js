import Cookies from 'js-cookie'

export const getCookie = (key) => {
  return Cookies.get(key)
}

export const setCookie = (key, value, attributes = { expires: 1 }) => {
  Cookies.set(key, value, attributes)
}

export const removeCookie = (key, attributes = {}) => {
  Cookies.remove(key, attributes)
}

export const removeCookieAll = (attributes = {}) => {
  Object.keys(Cookies.get()).forEach((cookie) => {
    Cookies.remove(cookie, attributes)
  })
}
