import axios from 'axios'
import { getCookie } from '../helpers/cookie'

const host = 'http://localhost:3350'
const auth = 'auth'

// const token = getCookie('token') != undefined ? getCookie('token') : getCookie('token')

axios.interceptors.request.use(
    function (config) {
        const token = getCookie('token');
        if (token) {
            config.headers["Authorization"] = "Bearer " + token;
        }
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

function Login(value) {
    const url = `${host}/login`
    return axios.post(url, value)
}

function Register(value) {
    const url = `${host}/register`
    return axios.post(url, {
        email: value.email,
        password: value.password
    })
}

function GetGalleryList(pr) {
    // console.log("tokentoken123: ", token);
    const url = `${host}/${auth}/galleries`
    return axios.get(url, {
        params: {
            limit: pr.limit,
            offset: pr.offset,
            user_id: pr.user_id
        }
    })
}

function UpdateUser(formData) {
    const url = `${host}/${auth}/user`
    return axios.patch(url, formData)
}

function CreateGallery(nameInput) {
    const url = `${host}/${auth}/gallery`
    return axios.post(url, { name: nameInput })
}

function CreateImagesByGalleryID(formData, galleryID) {
    const url = `${host}/${auth}/gallery-image/${galleryID}`
    return axios.post(url, formData)
}

export default {
    Login,
    Register,
    GetGalleryList,
    UpdateUser,
    CreateGallery,
    CreateImagesByGalleryID
}