const mainColor = '#4c3586';
const secondColor = '#456078';
const successColor = '#0ACF83';
const failColor = '#E8485B';

export {
    mainColor,
    secondColor,
    failColor,
    successColor,
};
