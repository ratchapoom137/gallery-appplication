import React from 'react'
import ReactDOM from 'react-dom'
import Slider from '@material-ui/core/Slider'
import Cropper from 'react-easy-crop'
// import '../App.css'

class Test extends React.Component {
    state = {
        imageSrc:
            'https://img.huffingtonpost.com/asset/5ab4d4ac2000007d06eb2c56.jpeg?cache=sih0jwle4e&ops=1910_1000',
        crop: { x: 0, y: 0 },
        zoom: 1,
        aspect: 1,
    }

    onCropChange = crop => {
        this.setState({ crop })
    }

    onCropComplete = (croppedArea, croppedAreaPixels) => {
        console.log(croppedAreaPixels.width / croppedAreaPixels.height)
    }

    onZoomChange = zoom => {
        this.setState({ zoom })
    }

    render() {
        return (
            <div style={{ width: '500px', height: '500px', marginTop: 20, backgroundColor:'red' }}>
                <div style={{ position: 'absolute', width: '500px', height: '500px' }}>
                    <Cropper
                        image={this.state.imageSrc}
                        crop={this.state.crop}
                        zoom={this.state.zoom}
                        aspect={this.state.aspect}
                        cropShape="round"
                        showGrid={false}
                        onCropChange={this.onCropChange}
                        onCropComplete={this.onCropComplete}
                        onZoomChange={this.onZoomChange}
                    />
                </div>
                <div style={{ position: 'absolute', top: '82%', width: '500px' }}>
                    <Slider
                        value={this.state.zoom}
                        min={1}
                        max={3}
                        step={0.1}
                        aria-labelledby="Zoom"
                        onChange={(e, zoom) => this.onZoomChange(zoom)}
                        classes={{ container: 'slider' }}
                    />
                </div>
            </div>
        )
    }
}

export default Test
