import React, { Component } from 'react';
import {
    Container,
    LGContentHeader,
    LGContentMiddle,
    LGContentFooter,
    CustomCol,
    LogoText
} from '../components/General.Style'
import {
    NeuDiv,
    NeuTextInput,
    NeuButton,
} from "neumorphism-react";
import InfoIcon from '@material-ui/icons/Info';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import ArrowBackIosOutlinedIcon from '@material-ui/icons/ArrowBackIosOutlined';
import { Switch } from 'neumorphic-react'
import {
    mainColor,
    failColor,
    successColor
} from '../constants/Color'
import { Form, message } from 'antd';
import {
    LOGIN_PAGE_PATH,
    PROFILE_PAGE_PATH
} from '../constants/RouteName'
import { setCookie, getCookie } from '../helpers/cookie'
import API from '../constants/API'

class RegisterPage extends Component {

    state = {
        showPassword: false,
        onFocusPassword: false,
        one: false,
        two: false,
        three: false,
        four: false,
        password: '',
        confirmPassword: '',
        email: ''
    }

    componentWillMount() {
        if (getCookie('user_profile') != undefined || getCookie('user_profile') != null) {
            this.props.history.push(PROFILE_PAGE_PATH)
        }
    }

    componentDidMount() {

    }

    onClickShowPassword = () => {
        const { showPassword } = this.state
        this.setState({
            showPassword: !showPassword
        })
    }

    navigrationByPath = (path) => {
        this.props.history.push(path)
    }

    onChangePassword = (value) => {
        var up = /([A-Z])/;
        var low = /([a-z])/;
        var num = /([0-9])/;
        var sp = /([!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])/;
        if (value != null || value != undefined) {
            if (num.test(value)) {
                this.setState({ four: true })
            } else {
                this.setState({ four: false })
            }
            if (up.test(value) && low.test(value)) {
                this.setState({ three: true })
            } else {
                this.setState({ three: false })
            }
            if (sp.test(value)) {
                this.setState({ two: true })
            } else {
                this.setState({ two: false })
            }
            if (value.length > 8) {
                this.setState({ one: true })
            } else {
                this.setState({ one: false })
            }
        } else {
            this.setState({
                one: false,
                two: false,
                three: false,
                four: false
            })
        }
    }

    onFinish = async (value) => {
        try {
            const response = await API.Register(value)
            // setCookie('user_profile', response.data.data)
            // const userProfile = getCookie('user_profile')
            this.props.history.push(LOGIN_PAGE_PATH)
            message.success('Register Successful.');
            // const userInfo = getCookie('user_profile')
            // const userProfile = JSON.parse(userInfo)

        } catch (err) {
            console.log("err: ", err);
            this.setState({ invalidAlert: true })
        }
    }

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    policyPassword = async (rule, value, callback) => {
        this.setState({
            password: value
        })
        var up = /([A-Z])/;
        var low = /([a-z])/;
        var num = /([0-9])/;
        var sp = /([!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])/;
        if (value != null || value != undefined) {
            if (!num.test(value)) {
                throw new Error(' ');
            }
            if (!up.test(value) && !low.test(value)) {
                throw new Error(' ');
            }
            if (!sp.test(value)) {
                throw new Error(' ');
            }
            if (value.length < 8) {
                throw new Error(' ');
            }
        }
    }

    checkConfirmPassword = async (role, value, callback) => {
        const { password } = this.state

        if (value != password && value != undefined) {
            throw new Error('Please make sure your password match.');
        }
    }

    render() {
        const { showPassword, onFocusPassword, one, two, three, four } = this.state
        // const { getFieldDecorator, getFieldValue } = this.props.form;
        return (
            <Container>
                <NeuDiv
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                    width="500px"
                    height="820px"
                    color="#F1F1F1"
                    column={true}
                    radius={16}
                >
                    <LGContentHeader hegiht="8%">

                        <NeuButton
                            style={{ right: 420, top: 16, position: 'absolute', alignItems: 'center', justifyContent: 'center', display: 'flex', flex: 1 }}
                            width="50px"
                            height="50px"
                            distance={2}
                            onClick={() => this.navigrationByPath(LOGIN_PAGE_PATH)}
                            color="#F1F1F1"
                        >
                            <ArrowBackIosOutlinedIcon />
                        </NeuButton>
                        <div>
                            <LogoText fontSize="40px" fontWeight="bold">
                                Poomstagram
                            </LogoText>
                        </div>

                    </LGContentHeader>

                    <LGContentMiddle height="70%" justifyContent="flex-start">
                        <div style={{ marginBottom: 30 }}>
                            <text style={{ fontWeight: 'bold', color: '#A9A9A9', fontSize: 20 }}>Sign In</text>
                        </div>
                        <Form
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                display: 'flex',
                                flexDirection: 'column',
                                height: '100%'
                            }}
                            name="signup"
                            onFinish={this.onFinish}
                            onFinishFailed={this.onFinishFailed}
                        >
                            <CustomCol>
                                <Form.Item
                                    name="email"
                                    rules={[
                                        {
                                            required: true,
                                            message: <text style={{ fontSize: 12, color: failColor }}>Please input your email.</text>,
                                        },
                                        {
                                            type: 'email',
                                            message: <text style={{ fontSize: 12, color: failColor }}>Please match the requested format.</text>,
                                        }
                                    ]}
                                >
                                    <NeuTextInput
                                        style={{ borderRadius: 30 }}
                                        placeholder="Email"
                                        color="#F1F1F1"
                                        onChange={(value) => this.setState({ email: value })}
                                        width="450px"
                                        height="74px"
                                        distance={5}
                                        fontSize={15}
                                        fontColor="#151515"
                                        autoFocus
                                    />
                                </Form.Item>
                            </CustomCol>
                            <CustomCol>
                                <Form.Item
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: <text style={{ fontSize: 12, color: failColor }}>Please input your password.</text>,
                                        },
                                        {
                                            validator: this.policyPassword,
                                        }
                                    ]}
                                >
                                    <NeuTextInput
                                        type={showPassword == false ? "password" : ""}
                                        style={{ borderRadius: 30 }}
                                        placeholder="Password"
                                        color="#F1F1F1"
                                        onChange={(value) => this.onChangePassword(value)}
                                        width="450px"
                                        height="74px"
                                        distance={5}
                                        fontSize={15}
                                        fontColor="#151515"
                                        onFocus={() => this.setState({ onFocusPassword: true })}
                                    // onBlur={() => this.setState({ onFocusPassword: false })}
                                    />
                                </Form.Item>
                            </CustomCol>
                            {
                                onFocusPassword == true ?
                                    <div
                                        style={{
                                            width: '400px',
                                            height: '150px'
                                        }}
                                    >
                                        <div style={{ alignItems: 'flex-start', display: 'flex' }}>
                                            <InfoIcon />
                                            <text style={{ marginLeft: 6, fontSize: 14, marginTop: 3 }}>
                                                Hints for a strong password:
                                            </text>
                                        </div>
                                        <div
                                            style={{
                                                flexDirection: 'column',
                                                display: 'flex',
                                                alignItems: 'flex-start',
                                                marginLeft: 40,
                                                color: failColor
                                            }}
                                        >
                                            <text
                                                style={{
                                                    fontSize: 14,
                                                    marginTop: 3,
                                                    color: one == false ? '' : successColor
                                                }}
                                            >
                                                <FiberManualRecordIcon style={{ fontSize: 14, marginRight: 8 }} /> At least 8 characters
                                        </text>
                                            <text
                                                style={{
                                                    fontSize: 14,
                                                    marginTop: 3,
                                                    color: two == false ? '' : successColor
                                                }}
                                            >
                                                <FiberManualRecordIcon style={{ fontSize: 14, marginRight: 8 }} /> At least one special character
                                        </text>
                                            <text
                                                style={{
                                                    fontSize: 14,
                                                    marginTop: 3,
                                                    color: three == false ? '' : successColor
                                                }}
                                            >
                                                <FiberManualRecordIcon style={{ fontSize: 14, marginRight: 8 }} /> At least one uppercase and one lower case letter
                                        </text>
                                            <text
                                                style={{
                                                    fontSize: 14,
                                                    marginTop: 3,
                                                    color: four == false ? '' : successColor
                                                }}
                                            >
                                                <FiberManualRecordIcon style={{ fontSize: 14, marginRight: 8 }} />  At least one number
                                        </text>
                                        </div>
                                    </div>
                                    : null
                            }
                            <CustomCol>
                                <Form.Item
                                    name="confirmPassword"
                                    rules={[
                                        {
                                            required: true,
                                            message: <text style={{ fontSize: 12, color: failColor }}>Please input confirm password.</text>,
                                        },
                                        {
                                            validator: this.checkConfirmPassword,
                                        }
                                    ]}
                                >
                                    <NeuTextInput
                                        type={showPassword == false ? "password" : ""}
                                        style={{ borderRadius: 30 }}
                                        placeholder="Confirm Password"
                                        color="#F1F1F1"
                                        onChange={(newValue) => console.log("newValue : ", newValue)}
                                        width="450px"
                                        height="74px"
                                        distance={5}
                                        onChange={(newValue) => console.log("newValue : ", newValue)}
                                        fontSize={15}
                                        fontColor="#151515"
                                    />
                                </Form.Item>
                            </CustomCol>
                            <div
                                style={{
                                    width: '450px',
                                    height: '70px',
                                    backgroundColor: mainColor,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    display: 'flex',
                                    borderRadius: 30,
                                    marginTop: 30,
                                }}
                            >
                                <NeuButton
                                    width="97%"
                                    height="89%"
                                    // width="90%"
                                    // height="40%"
                                    color={mainColor}
                                    // color="#F1F1F1"
                                    radius={30}
                                    distance={4}
                                    type="primary" htmlType="submit"
                                >
                                    <text style={{ fontWeight: 'bold', color: '#F1F1F1' }}>Register</text>
                                </NeuButton>
                            </div>
                        </Form>
                    </LGContentMiddle>
                    <LGContentFooter height="20%" justifyContent="center">

                    </LGContentFooter>

                </NeuDiv>
            </Container >
        )
    }
}

export default RegisterPage