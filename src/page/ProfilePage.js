import React, { Component } from 'react'
import Header from '../components/Header'
import Profile from '../components/Profile'
import {
    NeuDiv,
    NeuButton,
} from "neumorphism-react";
import {
    TextNeu,
    CardCustom,
    ImageCustom
} from '../components/General.Style'
import {
    mainColor,
    secondColor,
} from '../constants/Color'
import userLogo from '../assets/images/user.png'
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';
import MoreHorizOutlinedIcon from '@material-ui/icons/MoreHorizOutlined';
import PlaylistAddOutlinedIcon from '@material-ui/icons/PlaylistAddOutlined';
import { List, Input, Card, Empty, Modal, Upload, Row, Col, Spin, Form, message } from 'antd';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { getCookie, removeCookieAll } from '../helpers/cookie'
import API from '../constants/API'
import Image from 'react-image-resizer';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import BG from '../assets/images/266685.jpg'
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import { PlusOutlined, LoadingOutlined, UploadOutlined, InboxOutlined } from '@ant-design/icons';
import Button from '@material-ui/core/Button';
import PublishIcon from '@material-ui/icons/Publish';
import FittedImage from 'react-fitted-image';
import {
    failColor
} from '../constants/Color'
import Compress from 'compress.js'
import { DropzoneDialog, DropzoneArea } from 'material-ui-dropzone'

const { TextArea } = Input;
const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
const compress = new Compress()
const { Dragger } = Upload;

function getBase64(file) {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

class ProfilePage extends Component {

    state = {
        hover: [
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },
            { isHover: false, isDisale: false },

        ],
        visibleBio: false,
        offset: 0,
        galleryList: [],
        galleryTotal: 0,
        profileInfo: null,
        visibleAddNewGallery: false,
        add_img: [],
        createGalleryButtonSpining: false,
        fileList: [],
    }

    componentWillMount() {
        if (getCookie('user_profile') === undefined || getCookie('user_profile') === null) {
            this.props.history.push("/")
        } else {
            console.log("ldmasdasod: ", getCookie('user_profile'));
            this.getGalleryList()
        }
    }

    resizeImageFn = async (file) => {
        const resizedImage = await compress.compress([file], {
            size: 12, // the max size in MB, defaults to 2MB
            quality: 10, // the quality of the image, max is 1,
            maxWidth: 1280, // the max width of the output image, defaults to 1920px
            maxHeight: 800, // the max height of the output image, defaults to 1920px
            resize: true // defaults to true, set false if you do not want to resize the image width and height
        })
        const img = resizedImage[0];
        const base64str = img.data
        const imgExt = img.ext
        const resizedFiile = Compress.convertBase64ToFile(base64str, imgExt)
        return resizedFiile;
    }

    getGalleryList = async () => {
        try {
            let filter = {
                limit: 9,
                offset: this.state.offset,
            }
            if (this.props.location.state != undefined) {
                filter['user_id'] = this.props.location.state.user_id
            }

            const response = await API.GetGalleryList(filter)
            console.log("response3: ", response);
            let addHover = []
            response.data.data.gallery_list.map(gl => {
                gl.images.map(img => {
                    addHover.push({ isHover: false, isDisale: false })
                })
            })
            this.setState({
                galleryList: response.data.data.gallery_list,
                galleryTotal: response.data.total,
                profileInfo: {
                    image_total: response.data.data.img_total,
                    like_total: response.data.data.like_total
                }
            })
        } catch (err) {

        }
    }

    // handlePreview = async file => {
    //     if (!file.url && !file.preview) {
    //         console.log("file: ", file);
    //         file.preview = await getBase64(file.originFileObj);
    //     }
    // };

    handleChange = ({ fileList }) => this.setState({ fileList });

    handleFileChange = (e) => {
        e.preventDefault();
        let files = e.target.files;
        let test = []
        for (let i = 0; i < e.target.files.length; i++) {

            const fileBlob = this.resizeImageFn(files[i])
            var newFile = new File([fileBlob], files[i].name, { type: files[i].type, lastModified: Date.now() });
            test.push(files[i])
            console.log("files[i]: ", files[i]);
            console.log("newFile: ", newFile);
        }
        this.setState({
            add_img: test
        });
    }

    handleFileChange2 = (info) => {

    }

    onFinish = async (value) => {
        this.setState({
            createGalleryButtonSpining: true
        })
        if (this.state.visibleAddNewGallery != false) {
            let galleryID = 0
            try {
                const response = await API.CreateGallery(value['gallery_name'])
                galleryID = response.data.data
            } catch (err) {
                console.log(err);
            }
            const formData = new FormData();
            formData.append("test", 'testtt');
            Object.entries(value).map(([key, v]) => {
                if (key != 'gallery_name') {
                    console.log("key: ", key);
                    console.log("this.state.add_img[key]: ", this.state.add_img[key]);
                    formData.append("photos", this.state.add_img[key]);
                    formData.append("description", v);
                }
            })
            try {
                const response = await API.CreateImagesByGalleryID(formData, galleryID)
                this.setState({
                    visibleAddNewGallery: false,
                    createGalleryButtonSpining: false,
                    add_img: []
                })
                this.getGalleryList()
            } catch (err) {
                console.log(err);
            }
        }
    }

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    }

    render() {
        if (this.state.hover.length >= 1 && this.state.profileInfo != null) {
            const uploadButton = (
                <div>
                    <PlusOutlined />
                    <div className="ant-upload-text">Upload</div>
                </div>
            );
            // console.log("this.state.fileList: ", this.state.fileList);
            // console.log("imageesss: ", this.state.add_img);
            console.log("fileList: ", this.state.fileList);


            return (
                <div style={{ width: '100%', height: '100%', minHeight: '100vh', backgroundColor: '#F1F1F1' }}>
                    {/* <label>profile page</label> */}
                    <Header history={this.props.history} />
                    <div
                        style={{
                            width: '100%', minHeight: '100vh', height: '100%',
                            justifyContent: 'center', alignItems: 'center', display: 'flex',
                            backgroundColor: ''
                        }}
                    >
                        {/* Content */}
                        <NeuDiv
                            style={{
                                width: '90%', minHeight: '100vh', height: '100%',
                                marginTop: 24, marginBottom: 50,
                                justifyContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column'
                            }}
                            color="#F1F1F1" distance={6}
                        >
                            <div
                                style={{
                                    position: 'absolute', top: 0, backgroundColor: '', width: '100%', height: 404,
                                    justifyContent: 'center', alignItems: 'flex-start', display: 'flex',
                                }}
                                onMouseOver={() => console.log("qwpdqw")}
                            >
                                {/* <img
                                    width="80%" height={403}
                                    style={{
                                        resizeMode: "contain"
                                    }}
                                    src={BG}
                                    onMouseOver={() => console.log("qwpdqw")}
                                /> */}
                                <AddPhotoAlternateIcon style={{ position: 'absolute', right: 196, marginTop: 10, fontSize: 40, color: '#F1F1F1' }} />
                                {/* <Image
                                    src={BG}
                                    // style={{ backgroundColor: 'blue' }}
                                    width={1714}
                                    height={360}
                                /> */}
                            </div>
                            {/* Profile Content */}
                            <Profile galleryTotal={this.state.galleryTotal} profileInfo={this.state.profileInfo} />
                            <NeuDiv
                                radius={0}
                                distance={2}
                                revert
                                width="100%" height="8px" color="#F1F1F1"
                            >
                            </NeuDiv>
                            {/* Gallery Content */}
                            <div
                                style={{
                                    minHeight: '666px', height: '100%', width: '100%',
                                    backgroundColor: '#F1F1F1', borderRadius: 26,
                                }}
                            >
                                {/* Header */}
                                <div
                                    style={{
                                        height: '80px', width: '100%', backgroundColor: '',
                                        justifyContent: 'flex-start', alignItems: 'center', display: 'flex',
                                    }}
                                >
                                    <div
                                        style={{
                                            height: '100%', justifyContent: 'flex-start', alignItems: 'center', display: 'flex', marginTop: 16,
                                            width: '14%', backgroundColor: '', marginLeft: 30
                                        }}
                                    >
                                        <text style={{ fontSize: "23.5px", color: `${secondColor}`, fontWeight: "bold" }}>
                                            List of Gallery
                                        </text>
                                        {/* <TextNeu
                                            fontSize="28px" color={secondColor} fontWeight="bold"
                                        >
                                            {this.state.galleryTotal}
                                        </TextNeu> */}
                                    </div>
                                    <div style={{ height: '100%', justifyContent: 'flex-end', alignItems: 'flex-end', display: 'flex' }}>
                                        {/* <NeuButton
                                            style={{
                                                margin: 0, marginLeft: 34,
                                                justifyContent: 'center', alignItems: 'center', display: 'flex'
                                            }}
                                            width="194px"
                                            height="74%"
                                            onClick={() => console.log("Button cliked !")}
                                            color="#F1F1F1"
                                        >
                                            <PlaylistAddOutlinedIcon style={{ color: mainColor, marginRight: 4 }} />
                                            <text style={{ fontSize: "15px", color: `${mainColor}`, fontWeight: "bold" }}>Add New Gallery</text>
                                        </NeuButton> */}
                                        <div
                                            style={{
                                                width: "202px",
                                                height: "68px",
                                                backgroundColor: mainColor,
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                display: 'flex',
                                                borderRadius: 16
                                            }}
                                        >
                                            <NeuButton
                                                style={{
                                                    margin: 0,
                                                    justifyContent: 'center', alignItems: 'center', display: 'flex'
                                                }}
                                                width="194px" distance={4}
                                                height="60px"
                                                onClick={() => this.setState({ visibleAddNewGallery: true })}
                                                color={mainColor}
                                                radius={16}
                                            >
                                                <PlaylistAddOutlinedIcon style={{ color: '#ffffff', marginRight: 4 }} />
                                                <text style={{ fontSize: "15px", color: '#ffffff', fontWeight: "bold" }}>Add New Gallery</text>
                                            </NeuButton>
                                        </div>
                                    </div>
                                    <div
                                        style={{
                                            width: '80%', backgroundColor: '',
                                            justifyContent: 'flex-end', alignItems: 'center', display: 'flex'
                                        }}
                                    >
                                        <MoreHorizOutlinedIcon style={{ fontSize: '30px', color: 'black', marginRight: 24 }} />
                                    </div>
                                </div>
                                <div
                                    style={{
                                        backgroundColor: '', width: '100%', height: '100%', minHeight: '62vh',
                                        justifyContent: 'center', alignItems: 'flex-start', display: 'flex'
                                    }}
                                >

                                    <div
                                        style={{
                                            backgroundColor: '', width: '98%', height: '96%', marginTop: 30, minHeight: '58vh',
                                            justifyContent: 'flex-start', alignItems: 'flex-start', display: 'flex', flexDirection: 'row'
                                        }}
                                    >
                                        <List
                                            grid={{
                                                gutter: 16,

                                            }}
                                            dataSource={this.state.galleryList}
                                            renderItem={(item, index) => (
                                                <List.Item>
                                                    <CardCustom
                                                        style={{
                                                            height: "400px", width: 546.6, backgroundColor: 'white',
                                                            flexDirection: 'row', display: 'flex', marginBottom: 30,
                                                        }}
                                                    >
                                                        <NeuButton
                                                            style={{
                                                                margin: 0, flexDirection: 'row', display: 'flex', flexDirection: 'column',
                                                                justifyContent: 'flex-start', alignItems: 'flex-end', padding: 0
                                                            }}
                                                            distance={this.state.hover[index].isHover === false ? 6 : 16}
                                                            hoverable
                                                            radius={10}
                                                            width="546.6px"
                                                            height="400px"
                                                            disabled={this.state.hover[index].isDisale}
                                                            onClick={() => console.log("asdasdas")}
                                                            color="#F1F1F1"
                                                            onMouseOver={() => {
                                                                let newHover = this.state.hover
                                                                newHover[index].isHover = true
                                                                this.setState({ hover: newHover })
                                                            }}
                                                            onMouseLeave={() => {
                                                                let newHover = this.state.hover
                                                                newHover[index].isHover = false
                                                                this.setState({ hover: newHover })
                                                            }}
                                                        >
                                                            {
                                                                this.state.hover[index].isHover === false ? null :
                                                                    <NeuButton
                                                                        style={{
                                                                            position: 'absolute', marginTop: 6,
                                                                            justifyContent: 'center', alignItems: 'center', display: 'flex',
                                                                        }}
                                                                        distance={4}
                                                                        radius={30}
                                                                        width="30px"
                                                                        height="30px"
                                                                        onClick={(e) => { e.stopPropagation(); console.log("555555") }}
                                                                        color="#F1F1F1"
                                                                        onMouseOver={() => {
                                                                            let newHover = this.state.hover
                                                                            newHover[index].isDisale = true
                                                                            this.setState({ hover: newHover })
                                                                        }}
                                                                        onMouseLeave={() => {
                                                                            let newHover = this.state.hover
                                                                            newHover[index].isDisale = false
                                                                            this.setState({ hover: newHover })
                                                                        }}
                                                                    >
                                                                        <MoreVertIcon style={{}} />
                                                                    </NeuButton>
                                                            }
                                                            {
                                                                item.images.length <= 0 ?
                                                                    <Empty
                                                                        style={{
                                                                            backgroundColor: '', width: '100%', height: '90%', flexDirection: 'column',
                                                                            justifyContent: 'center', alignItems: 'center', display: 'flex'
                                                                        }}
                                                                    /> :
                                                                    <div
                                                                        style={{
                                                                            flexDirection: 'row', display: 'flex', width: '100%', height: '90%',
                                                                            justifyContent: 'center', alignItems: 'center',
                                                                        }}
                                                                    >
                                                                        <div
                                                                            style={{
                                                                                backgroundColor: '',
                                                                                justifyContent: 'center', alignItems: 'flex-start', display: 'flex', flexDirection: 'column'
                                                                            }}
                                                                        >
                                                                            {/* <Image
                                                                                src={
                                                                                    item.images[index] != undefined && item.images.length >= 1 ?
                                                                                        `http://localhost:3350/${item.images[0].url}` : null
                                                                                }
                                                                                // style={{ backgroundColor: 'blue' }}
                                                                                width={item.images.length <= 2 ? 490 : 245}
                                                                                height={item.images.length <= 1 ? 324 : 162}
                                                                            /> */}
                                                                            <FittedImage
                                                                                fit="cover"
                                                                                style={{ width: item.images.length <= 2 ? 490 : 245, height: item.images.length <= 1 ? 324 : 162 }}
                                                                                loader={
                                                                                    <div
                                                                                        style={{
                                                                                            width: item.images.length <= 2 ? 490 : 245, height: 162, justifyContent: 'center',
                                                                                            alignItems: 'center', display: 'flex'
                                                                                        }}
                                                                                    >
                                                                                        <Spin tip="Loading..." />
                                                                                    </div>
                                                                                }
                                                                                onLoad={(...args) => console.log(...args)}
                                                                                onError={(...args) => console.log(...args)}
                                                                                src={
                                                                                    item.images[index] != undefined && item.images.length >= 1 ?
                                                                                        `http://localhost:3350/${item.images[0].url}` : null
                                                                                }
                                                                            />
                                                                            {
                                                                                item.images.length >= 2 ?
                                                                                    // <Image
                                                                                    //     src={
                                                                                    //         item.images[index] != undefined && item.images.length >= 1 ?
                                                                                    //             `http://localhost:3350/${item.images[1].url}` : null
                                                                                    //     }
                                                                                    //     // style={{ backgroundColor: 'blue' }}
                                                                                    //     width={item.images.length <= 2 ? 490 : 245}
                                                                                    //     height={162}
                                                                                    // />
                                                                                    <FittedImage
                                                                                        fit="cover"
                                                                                        style={{ width: item.images.length <= 2 ? 490 : 245, height: 162 }}
                                                                                        loader={
                                                                                            <div
                                                                                                style={{
                                                                                                    width: item.images.length <= 2 ? 490 : 245, height: 162, justifyContent: 'center',
                                                                                                    alignItems: 'center', display: 'flex'
                                                                                                }}
                                                                                            >
                                                                                                <Spin tip="Loading..." />
                                                                                            </div>
                                                                                        }
                                                                                        onLoad={(...args) => console.log(...args)}
                                                                                        onError={(...args) => console.log(...args)}
                                                                                        src={
                                                                                            item.images[index] != undefined && item.images.length >= 1 ?
                                                                                                `http://localhost:3350/${item.images[1].url}` : null
                                                                                        }
                                                                                    />
                                                                                    : null
                                                                            }
                                                                        </div>
                                                                        {
                                                                            item.images.length >= 3 ?
                                                                                <div
                                                                                    style={{
                                                                                        backgroundColor: '',
                                                                                        justifyContent: 'center', alignItems: 'flex-start', display: 'flex', flexDirection: 'column'
                                                                                    }}
                                                                                >
                                                                                    {/* <Image
                                                                                        style={{ position: 'absolute' }}
                                                                                        src={
                                                                                            item.images[index] != undefined && item.images.length >= 1 ?
                                                                                                `http://localhost:3350/${item.images[2].url}` : null
                                                                                        }
                                                                                        // style={{ backgroundColor: 'blue' }}
                                                                                        width={245}
                                                                                        height={162}
                                                                                    /> */}
                                                                                    <FittedImage
                                                                                        fit="cover"
                                                                                        style={{ width: 245, height: item.images.length === 3 ? 324 : 162 }}
                                                                                        loader={
                                                                                            <div
                                                                                                style={{
                                                                                                    width: 245, height: item.images.length === 3 ? 324 : 162, justifyContent: 'center',
                                                                                                    alignItems: 'center', display: 'flex'
                                                                                                }}
                                                                                            >
                                                                                                <Spin tip="Loading..." />
                                                                                            </div>
                                                                                        }
                                                                                        onLoad={(...args) => console.log(...args)}
                                                                                        onError={(...args) => console.log(...args)}
                                                                                        src={
                                                                                            item.images[index] != undefined && item.images.length >= 1 ?
                                                                                                `http://localhost:3350/${item.images[2].url}` : null
                                                                                        }
                                                                                    />
                                                                                    {
                                                                                        item.images.length >= 4 ?
                                                                                            <FittedImage
                                                                                                fit="cover"
                                                                                                style={{ width: 245, height: 162 }}
                                                                                                loader={
                                                                                                    <div
                                                                                                        style={{
                                                                                                            width: 245, height: 162, justifyContent: 'center',
                                                                                                            alignItems: 'center', display: 'flex'
                                                                                                        }}
                                                                                                    >
                                                                                                        <Spin tip="Loading..." />
                                                                                                    </div>
                                                                                                }
                                                                                                onLoad={(...args) => console.log(...args)}
                                                                                                onError={(...args) => console.log(...args)}
                                                                                                src={
                                                                                                    item.images[index] != undefined && item.images.length >= 1 ?
                                                                                                        `http://localhost:3350/${item.images[3].url}` : null
                                                                                                }
                                                                                            /> : null
                                                                                    }

                                                                                </div> : null
                                                                        }

                                                                    </div>
                                                            }


                                                            <div
                                                                style={{
                                                                    backgroundColor: '', width: '90%', height: '10%', marginRight: 29,
                                                                    justifyContent: 'flex-end', alignItems: 'flex-start', display: 'flex',
                                                                }}
                                                            >
                                                                <div
                                                                    style={{
                                                                        width: '50%',
                                                                        justifyContent: 'flex-start', alignItems: 'center', display: 'flex',
                                                                    }}
                                                                >
                                                                    <text style={{ fontSize: "15px", color: '#585858', fontWeight: "bold" }}>
                                                                        Name:{`\xa0\xa0`}
                                                                    </text>
                                                                    <text style={{ fontSize: "15px", color: '#262626', fontWeight: "bold" }}>
                                                                        {item.name}
                                                                    </text>
                                                                </div>
                                                                <div
                                                                    style={{
                                                                        width: '50%',
                                                                        justifyContent: 'flex-end', alignItems: 'center', display: 'flex',
                                                                    }}
                                                                >
                                                                    <text style={{ fontSize: "15px", color: secondColor, fontWeight: "bold" }}>
                                                                        {item.images_total} Image
                                                                     </text>
                                                                </div>

                                                            </div>
                                                        </NeuButton>
                                                    </CardCustom>
                                                </List.Item>
                                            )}
                                        />
                                    </div>
                                </div>

                            </div>
                        </NeuDiv>

                    </div>
                    <Modal
                        width="60%"
                        title={
                            <TextNeu fontSize="20px" color={mainColor} fontWeight="bold">
                                Add New Gallery
                            </TextNeu>
                        }
                        visible={this.state.visibleAddNewGallery}
                        onCancel={() => this.setState({ visibleAddNewGallery: false })}
                        footer={false}
                    // onOk={this.handleOk}
                    // onCancel={this.handleCancel}
                    >
                        <div style={{
                            backgroundColor: '', width: '100%', height: '100%',
                            justifyContent: '', alignItems: '', display: 'flex',
                            flexDirection: 'column'
                        }}>
                            <Form
                                name="upload_image"
                                onFinish={this.onFinish}
                                onFinishFailed={this.onFinishFailed}
                            >
                                <div>
                                    <label style={{ marginRight: 10 }}>Gallery Name: </label>
                                    <Form.Item
                                        name='gallery_name'
                                        rules={[
                                            {
                                                required: true,
                                                message: <text style={{ fontSize: 12, color: failColor }}>Please input your gallery name.</text>,
                                            },
                                        ]}
                                    >
                                        <Input
                                            // defaultValue={this.state.userLogin.first_name}
                                            placeholder="Enter your gallery name"
                                            autoFocus
                                            style={{
                                                borderRadius: 30,
                                                borderColor: '#ffffff',
                                                backgroundColor: '#ffffff',
                                                boxSizing: 'border-box',
                                                border: '4px solid #ffffff',
                                                boxShadow: '2px 2px 4px #d1d1d1, -2px -2px 4px #ffffff, inset 2px 2px 4px #d1d1d1, inset -2px -2px 4px #ffffff',
                                                width: '20%', height: '50px',
                                                marginBottom: 10,
                                            }}
                                        // onChange={(value) => this.setState({ first_name_edit: value.target.value })}
                                        />
                                    </Form.Item>
                                </div>
                                <div
                                    style={{
                                        justifyContent: 'flex-start', alignItems: 'center', width: '100%', height: 'auto', backgroundColor: ''
                                    }}
                                >
                                    <input onChange={this.handleFileChange} accept="image/*" style={{ display: 'none' }} id="contained-button-file" type="file" multiple={true} />
                                    <label htmlFor="contained-button-file">
                                        <Button style={{ marginTop: 0, marginBottom: 20 }} variant="contained" color="primary" component="span">
                                            <UploadOutlined style={{ marginRight: 10 }} /> Upload Images
                                        </Button>
                                    </label>
                                    <Dragger
                                        name='file'
                                        multiple={true}
                                        action='https://www.mocky.io/v2/5cc8019d300000980a055e76'
                                        showUploadList={false}
                                        onChange={}
                                    >
                                        <p className="ant-upload-drag-icon">
                                            <InboxOutlined />
                                        </p>
                                        <p className="ant-upload-text">Click or drag file to this area to upload</p>
                                        <p className="ant-upload-hint">
                                            Support for a single or bulk upload. Strictly prohibit from uploading company data or other
                                            band files
                                        </p>
                                    </Dragger>
                                    <div style={{ width: '100%', height: 'auto', backgroundColor: '' }}>

                                        <Row gutter={[16, 24]}>
                                            {
                                                this.state.add_img.length > 0 ?
                                                    this.state.add_img.map((item, index) => {
                                                        return (
                                                            <Col className="gutter-row" span={8} style={{ backgroundColor: '' }}>
                                                                <FittedImage
                                                                    fit="cover"
                                                                    style={{ width: 360, height: 300 }}
                                                                    loader={
                                                                        <div
                                                                            style={{
                                                                                width: 360, height: 300, justifyContent: 'center',
                                                                                alignItems: 'center', display: 'flex'
                                                                            }}
                                                                        >
                                                                            <Spin tip="Loading..." />
                                                                        </div>
                                                                    }
                                                                    onLoad={(...args) => console.log(...args)}
                                                                    onError={(...args) => console.log(...args)}
                                                                    src={URL.createObjectURL(item)}
                                                                />
                                                                <Form.Item
                                                                    name={`${index}`}
                                                                >
                                                                    <TextArea
                                                                        autoSize={{ minRows: 3, maxRows: 3 }}
                                                                        placeholder="Enter Your Bio..."
                                                                        // onChange={(value) => this.setState({ bioInput: value.target.value })}
                                                                        style={{
                                                                            borderRadius: 8,
                                                                            width: '100%',
                                                                            backgroundColor: '#ffffff',
                                                                            borderColor: '#ffffff',
                                                                            boxShadow: 'inset 4px 4px 8px #D5D5D5, inset -5px -5px 10px #ffffff'
                                                                        }}
                                                                    />
                                                                </Form.Item>
                                                            </Col>

                                                        )
                                                    }) : null
                                            }
                                        </Row>
                                    </div>
                                    {/* <img src={this.state.add_img != null ? this.state.add_img.imageSrc : ''} /> */}
                                </div>
                                <div
                                    style={{
                                        justifyContent: 'center', alignItems: 'center', display: 'flex',
                                        flexDirection: 'row', width: '100%', height: '100%', backgroundColor: ''
                                    }}
                                >

                                </div>
                                <div style={{ marginTop: 24, flexDirection: 'row', display: 'flex', backgroundColor: '', justifyContent: 'flex-end' }}>
                                    {/* <NeuButton
                                        style={{
                                            margin: 0, marginRight: 10,
                                            justifyContent: 'center', alignItems: 'center', display: 'flex',
                                        }}
                                        distance={2}
                                        width="100px"
                                        height="36px"
                                        htmlType="button"
                                        onClick={() => this.setState({ visibleAddNewGallery: false })}
                                        color="#F1F1F1"
                                    >
                                        <text
                                            style={{ fontSize: "15px", color: `${mainColor}`, fontWeight: "500" }}
                                        >
                                            Cancel
                            </text>
                                    </NeuButton> */}
                                    <div
                                        style={{
                                            width: "100%",
                                            height: "36px",
                                            backgroundColor: mainColor,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            display: 'flex',
                                            borderRadius: 12
                                        }}
                                    >
                                        <NeuButton
                                            style={{
                                                margin: 0,
                                                justifyContent: 'center', alignItems: 'center', display: 'flex',
                                            }}
                                            radius={12}
                                            distance={2}
                                            width="99%"
                                            height="28px"
                                            // onClick={() => this.saveProfile()}
                                            color={mainColor}
                                            type="primary" htmlType="submit"
                                        >
                                            <text
                                                style={{ fontSize: "15px", color: '#ffffff', fontWeight: "500" }}
                                            >
                                                <Spin indicator={antIcon} spinning={this.state.createGalleryButtonSpining} />{`\xa0`}{`\xa0`}Save
                                </text>
                                        </NeuButton>

                                    </div>
                                </div>
                            </Form>
                        </div>
                    </Modal>
                </div >
            )
        } else {
            return (
                <div></div>
            )
        }

    }
}

export default ProfilePage
