import React, { Component } from 'react';
import {
    Container,
    LGContentHeader,
    LGContentMiddle,
    LGContentFooter,
    AlertNeu,
    CustomCol,
    TextNeu,
    LogoText
} from '../components/General.Style'
import {
    NeuDiv,
    NeuTextInput,
    NeuButton,
    NeuToggle,
} from "neumorphism-react";
import VisibilityIcon from '@material-ui/icons/Visibility';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import CloseIcon from '@material-ui/icons/Close';
import ErrorIcon from '@material-ui/icons/Error';
import {
    mainColor,
    secondColor,
    failColor
} from '../constants/Color'
import {
    REGISTER_PAGE_PATH,
    PROFILE_PAGE_PATH
} from '../constants/RouteName'
import Collapse from '@material-ui/core/Collapse';
import { Form } from 'antd';
import { setCookie, getCookie } from '../helpers/cookie'
import API from '../constants/API'

class LoginPage extends Component {

    state = {
        showPassword: false,
        invalidAlert: false,
        email: '',
        password: '',
    }

    componentWillMount() {
        if (getCookie('user_profile') != undefined || getCookie('user_profile') != null) {
            this.props.history.push(PROFILE_PAGE_PATH)
        }
    }

    onClickShowPassword = () => {
        const { showPassword } = this.state
        this.setState({
            showPassword: !showPassword,
        })
    }

    navigrationByPath = (path) => {
        this.props.history.push(path)
    }

    onFinish = async (value) => {
        try {
            const response = await API.Login(value)
            setCookie('user_profile', response.data.data)
            setCookie('token', response.data.data.token)

            this.props.history.push(PROFILE_PAGE_PATH)
        } catch (err) {
            console.log("err: ", err);
            this.setState({ invalidAlert: true })
        }
    }

    onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    }

    render() {
        const { invalidAlert } = this.state
        return (
            <Container>
                <NeuDiv
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                    width="500px"
                    height="800px"
                    color="#F1F1F1"
                    column={true}
                    radius={16}
                >
                    <LGContentHeader>
                        {/* <img width="160px" src={logo} alt="Logo" /> */}
                        <LogoText fontSize="40px" fontWeight="bold">
                            Poomstagram
                        </LogoText>
                    </LGContentHeader>
                    <Collapse in={invalidAlert} style={{ borderRadius: '8px' }}>
                        <div style={{ marginBottom: 20, display: 'flex', flexDirection: 'row' }}>
                            <AlertNeu
                                message={
                                    <div style={{ alignItems: 'center', justifyContent: 'center', display: 'flex' }}>
                                        {/* <div style={{ alignItems: 'center', justifyContent: 'center', display: 'flex' }}> */}
                                        <ErrorIcon style={{ marginRight: 10 }} />
                                        <text style={{ marginRight: 8 }}>Invalid email or password. Please try again</text>
                                        <CloseIcon
                                            style={{ fontSize: 18, cursor: 'pointer' }}
                                            onClick={() => this.setState({ invalidAlert: false })}
                                        />
                                    </div>
                                }
                            />
                        </div>
                    </Collapse>
                    <LGContentMiddle height='80%'>
                        <TextNeu fontSize="20px" fontWeight="bold">
                            Sign In
                        </TextNeu>
                        <Form
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                display: 'flex',
                                flexDirection: 'column',
                                height: '100%'
                            }}
                            name="signup"
                            onFinish={this.onFinish}
                            onFinishFailed={this.onFinishFailed}
                        >
                            <CustomCol
                            // style={{ backgroundColor: 'blue', height: 100 }}
                            >
                                <Form.Item
                                    // style={{ backgroundColor: 'green', height: 100 }}
                                    name="email"
                                    rules={[
                                        {
                                            required: true,
                                            message: <text style={{ fontSize: 12, color: failColor }}>Please input your email.</text>,
                                        },
                                    ]}
                                >
                                    <NeuTextInput
                                        style={{ marginTop: 20, marginBottom: 8, borderRadius: 30 }}
                                        placeholder="Email"
                                        color="#F1F1F1"
                                        // onChange={(value) => this.setState({ email: value })}
                                        width="450px"
                                        height="74px"
                                        distance={5}
                                        fontSize={15}
                                        fontColor="#151515"
                                        autoFocus
                                    />
                                </Form.Item>
                            </CustomCol>
                            <CustomCol>
                                <Form.Item
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: <text style={{ fontSize: 12, color: failColor }}>Please input your password.</text>,
                                        },
                                    ]}
                                >
                                    <NeuTextInput
                                        type={this.state.showPassword == false ? "password" : ""}
                                        style={{ marginTop: 24, marginBottom: 8, borderRadius: 30 }}
                                        placeholder="Password"
                                        color="#F1F1F1"
                                        // onChange={(value) => this.setState({ password: value })}
                                        width="450px"
                                        height="74px"
                                        distance={5}
                                        fontSize={15}
                                        fontColor="#151515"
                                    />
                                </Form.Item>
                            </CustomCol>
                            <div
                                style={{
                                    marginTop: 50,
                                    width: '450px',
                                    height: '70px',
                                    backgroundColor: mainColor,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    display: 'flex',
                                    borderRadius: 30
                                }}
                            >
                                <NeuButton
                                    width="97%"
                                    height="89%"
                                    // onClick={() => this.onClickLoginButton()}
                                    color={mainColor}
                                    radius={30}
                                    distance={4}
                                >
                                    <text style={{ fontWeight: 'bold', color: '#F1F1F1' }}>Log in</text>
                                </NeuButton>
                            </div>
                        </Form>
                        <div>
                            <NeuButton
                                style={{
                                    // position: 'absolute',
                                    left: '240%',
                                    bottom: '406%',
                                    borderRadius: 26,
                                    background:
                                        this.state.showPassword == false ?
                                            "" : 'linear-gradient(145deg, #d9d9d9, #ffffff)',
                                }}
                                // distance={5}
                                width="74px"
                                height="56px"
                                onClick={() => this.onClickShowPassword()}
                                color="#F1F1F1"
                            >
                                {
                                    this.state.showPassword == false ?
                                        <VisibilityIcon />
                                        :
                                        <VisibilityOffIcon style={{ color: '#5f4a8b' }} />
                                }
                            </NeuButton>
                        </div>
                    </LGContentMiddle>
                    <LGContentFooter height='30%'>
                        <div style={{ margin: 0 }}>
                            <text style={{ color: secondColor, fontSize: 16 }}>Forgot your password?</text>
                        </div>
                        <NeuButton
                            width="90%"
                            height="70px"
                            onClick={() => this.navigrationByPath(REGISTER_PAGE_PATH)}
                            color="#F1F1F1"
                            radius={30}
                            distance={6}
                        >
                            <text style={{ color: secondColor, fontSize: 16 }}>Don't have an account yet? </text>
                            <text style={{ fontWeight: 'bold', color: secondColor, fontSize: 16 }}>Sign up!</text>
                            <text></text>
                        </NeuButton>
                    </LGContentFooter>
                </NeuDiv>

            </Container >
        )
    }
}

export default LoginPage