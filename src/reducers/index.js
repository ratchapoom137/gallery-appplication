export default (
    state = {
        user_login: {},
    },
    action
) => {
    switch (action.type) {
        case 'login':
            return {
                user_login: action.payload
            };
        default:
            return state
    }
}