import React from 'react'
import { Route, Switch } from 'react-router-dom'
import LoginPage from '../page/LoginPage'
import RegisterPage from '../page/RegisterPage'
import ProfilePage from '../page/ProfilePage'
import Test from '../page/Test'

function Routes() {
    return (
        <div style={{ width: '100%' }}>
            <Switch> {/* ต้องใส่ switch มาคอบ route เพราะว่าไม่งั้น component จะซ้อนกัน ต้องใส่ switch เพื่อสลับไปอีกหน้า */}
                <Route exact path="/" component={LoginPage} />
                <Route exact path="/signup" component={RegisterPage} />
                <Route exact path="/profile" component={ProfilePage} />
                <Route exact path="/test" component={Test} />
                {/* <Route component={MainPage} /> */} */}
            </Switch>
        </div>
    )
}

export default Routes